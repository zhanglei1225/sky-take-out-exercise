package com.sky.utils;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * excel 导出的数据格式
 */

@Data
public class ExcelData {

    public List<Map<String, Object>> dataList;

    public Map<String, Object> Maps;

    public Map<String,Map<String, Object>> sub; //主表中的子类数据

    public ExcelData(List<Map<String, Object>> dataList){
        this.dataList = dataList;
    }

    public ExcelData(List<Map<String, Object>> dataList, Map<String,Map<String, Object>> sub){
        this.dataList = dataList;
        this.sub =sub;
    }




}
