package com.sky.utils;

import lombok.Data;

import java.util.List;

/**
 * @author denghaibo
 * @date 2023/2/28 10:14
 */
@Data
public class ExcelHead<T> {


    //内容里的字段名称
    private String fieldName;
    //显示值，一般为中文的
    private String title;
    private T nullValue; //如果为null的值

    List<ExcelHead> subList;

    public ExcelHead() {

    }
    public ExcelHead(String fieldName, String title) {
        this.fieldName = fieldName;
        this.title = title;
    }

    public ExcelHead(String fieldName, String title, List<ExcelHead> subList) {
        this.fieldName = fieldName;
        this.title = title;
        this.subList = subList;
    }

    public ExcelHead(String fieldName, String title, T nullValue) {
        this.fieldName = fieldName;
        this.title = title;
        this.nullValue = nullValue;
    }





}
