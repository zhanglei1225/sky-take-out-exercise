package com.sky.utils;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yangyang
 * @version 1.0
 * @date 2023/8/8 下午6:45
 * @description
 */
@Slf4j
public class EasyExcelUtil<T> {

    private static final int MAX_SIZE = 1000;


    /**
     * @param response
     * @param cls
     * @param fileName
     * @param data
     * @param <T>
     * @throws IOException
     */
    public static <T> void downloadExcel(HttpServletResponse response, Class cls, String fileName, List<T> data) throws IOException {
        // 如果传入的data数据是空的话就让data成为一个空集合 变成下载导入模板
        if (CollUtil.isEmpty(data)) {
            data = new ArrayList<>();
        }
        try (OutputStream os = response.getOutputStream()) {
            setResponseHeader(response, fileName);
            EasyExcel.write(os, cls).sheet(fileName).doWrite(data);
        } catch (IOException e) {
            log.error("下载导入模板异常{}", e);
            throw new IOException("下载导入模板异常");
        }
    }

    /**
     * 功能：动态表头导出
     * @param response
     * @param fileName
     * @param data
     * @param <T>
     * @throws IOException
     */
    public static <T> void downloadExcel(HttpServletResponse response, String fileName, List<T> data) throws IOException {
        // 如果传入的data数据是空的话就让data成为一个空集合 变成下载导入模板
        if (CollUtil.isEmpty(data)) {
            data = new ArrayList<>();
        }
        try (OutputStream os = response.getOutputStream()) {
            setResponseHeader(response, fileName);
            // EasyExcel.write(os, cls).sheet(fileName).doWrite(data);
        } catch (IOException e) {
            log.error("下载导入模板异常{}", e);
            throw new IOException("下载导入模板异常");
        }
    }

    /**
     * @param response
     * @param fileName
     * @throws UnsupportedEncodingException
     */
    private static void setResponseHeader(HttpServletResponse response, String fileName) throws UnsupportedEncodingException {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        fileName = URLEncoder.encode( fileName,"UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename="+fileName+".xlsx");
    }

    /**
     * 解析excel和批量插入 调用ServiceImpl的默认方法进行插入操作
     *
     * @param file excel文件
     * @param cls  cls
     * @param impl impl
     */
    public static <T> int parseExcelAndDoInsertBatch(MultipartFile file, Class cls, ServiceImpl impl) throws IOException {
        List<T> resultList = new ArrayList<>();
        try (InputStream inputStream = file.getInputStream()) {
            EasyExcel.read(inputStream, cls, new ReadListener<T>() {
                @Override
                public void invoke(T t, AnalysisContext analysisContext) {
                    resultList.add(t);
                }

                @Override
                public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                }
            }).sheet().doRead();
            if (CollUtil.isNotEmpty(resultList)) {
                List<List<T>> partition = Lists.partition(resultList, MAX_SIZE);
                partition.forEach(impl::saveBatch);
            }
            return resultList.size();
        }
    }

    /**
     * 解析excel返回解析数据 解析数据之后如果有相关的操作建议使用此方法 如使用mapper的自动填充字段等
     *
     * @param file 文件                       前端传入文件
     * @param cls  cls                       需要解析的类的对象
     * @return {@link List}<{@link T}>       返回解析之后的泛型集合
     * @throws IOException ioexception       获取MultipartFile输入流有可能会导致IO异常
     */
    public static <T> List<T> parseExcel(MultipartFile file, Class cls) throws IOException {
        try (InputStream inputStream = file.getInputStream()) {
            List<T> resultList = new ArrayList<>();
            EasyExcel.read(inputStream, cls, new ReadListener<T>() {
                @Override
                public void invoke(T t, AnalysisContext analysisContext) {
                    resultList.add(t);
                }

                @Override
                public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                    log.info("解析数据完成-{}\n", resultList);
                }
            }).sheet().doRead();
            return resultList;
        }
    }

    /**
     * @param inputStream
     * @param cls
     * @return
     * @throws IOException
     */
    public static <T> List<T> parseExcel(InputStream inputStream, Class cls) throws IOException {
        List<T> resultList = new ArrayList<>();
        EasyExcel.read(inputStream, cls, new ReadListener<T>() {
            @Override
            public void invoke(T t, AnalysisContext analysisContext) {
                resultList.add(t);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                log.info("解析数据完成-{}\n", resultList);
            }
        }).sheet().doRead();
        return resultList;
    }


    /**
     * 功能：表头转化成excel 可以识别的数据
     * @param headList
     * @return
     */
    private static List<List<String>> convertHead(List<ExcelHead> headList) {
        List<List<String>> list = new ArrayList<>();
        for (ExcelHead head : headList) {
            if(head.getSubList()!=null && head.getSubList().size()>0){ //合并表头
                List<ExcelHead> ez = head.getSubList();
                List<String> head3;
                for(ExcelHead eh:ez){
                    head3 = new ArrayList<>();
                    head3.add(head.getTitle());
                    head3.add(eh.getTitle());
                    list.add(head3);
                }
            }else{
                list.add(Lists.newArrayList(head.getTitle()));
            }

        }
        //沒有搞清楚head的参数为List<List<String>>,用List<String>就OK了
        return list;
    }

    /**
     * 功能：表头和数据配对
     * @param headList
     * @param dataList key为head里的fieldName
     * @return
     */
  /*  private static List<List<Object>> convertData(List<ExcelHead> headList, List<Map<String, Object>> dataList) {
        List<List<Object>> result = new ArrayList();
        //对dataList转为easyExcel的数据格式
        for (Map<String, Object> data : dataList) {
            List<Object> row = new ArrayList();
            for (ExcelHead h : headList) {
                Object o = data.get(h.getFieldName());
                //需要对null的处理，比如age的null，要转为-1
                row.add(handler(o, h.getNullValue()));
            }
            result.add(row);
        }
        return result;
    }*/


    /**
     * 功能：表头和数据配对
     * @param headList
     * @param dataList key为head里的fieldName
     * @return
     */
    private static List<List<Object>> convertData(List<ExcelHead> headList,List<ExcelData> dataList) {
        List<List<Object>> result = new ArrayList();
        //对dataList转为easyExcel的数据格式
        for (ExcelData excelData : dataList) {
            List<Object> row = new ArrayList();
            for(Map<String, Object> data : excelData.getDataList()){
                for (ExcelHead h : headList) {
                    if(h.getSubList()!=null && h.getSubList().size()>0){
                        //Map<String, Object> Lis= (Map<String, Object>)data.get(h.getFieldName());
                        //List<Object> aList = convertDataEr(h.getList(),Lis);
                        //row.add(aList);
                        Map<String, Object> subData = new HashMap<>();
                        if(excelData.getSub()!=null){
                            subData= (Map<String, Object>)excelData.getSub().get(h.getFieldName());
                        }
                        List<ExcelHead> subList = h.getSubList();
                        for(ExcelHead head1:subList){
                            Object o ="";
                            if(subData!=null && subData.size()>0){
                                o = subData.get(head1.getFieldName());
                            }
                            //需要对null的处理，比如age的null，要转为-1
                            row.add(handler(o, head1.getNullValue()));
                        }
                    }else {
                        Object o = data.get(h.getFieldName());
                        //需要对null的处理，比如age的null，要转为-1
                        row.add(handler(o, h.getNullValue()));
                    }
                }
                result.add(row);
            }
        }
        return result;
    }

    /**
     * 功能：处理子类数据
     * @return
     */
    private static List<Object> convertDataEr(List<ExcelHead> list,Map<String, Object> data){
        List<Object> aList = new ArrayList<>();
        for(ExcelHead head:list){
            Object o ="";
            if(data!=null && data.size()>0){
                o = data.get(head.getFieldName());
            }
            //需要对null的处理，比如age的null，要转为-1
            aList.add(handler(o, head.getNullValue()));
        }
        return aList;
    }


    /**
     * null值处理
     *
     * @param o
     * @param nullValue
     * @return
     */
    private static Object handler(Object o, Object nullValue) {
        return o != null ? o : nullValue;
    }



    /**
     * 写excel
     * @param filePath 保存的路径名
     * @param headList
     * @param dataList
     */
  /*  public static void write(String filePath, List<ExcelHead> headList, List<Map<String, Object>> dataList) {
        ExcelWriterBuilder writerBuilder = EasyExcel.write();
        writerBuilder.file(filePath);
        writerBuilder.excelType(ExcelTypeEnum.XLSX);
        writerBuilder.autoCloseStream(true);
        writerBuilder.head(convertHead(headList)).sheet("sheet1")
                .doWrite(convertData(headList, dataList));
    }
*/
    /**
     * 写excel
     * @param filePath 保存的路径名
     * @param headList
     * @param dataList
     */
    public static void write(String filePath, List<ExcelHead> headList, List<ExcelData> dataList) {
        ExcelWriterBuilder writerBuilder = EasyExcel.write();
        writerBuilder.file(filePath);
        writerBuilder.excelType(ExcelTypeEnum.XLSX);
        writerBuilder.autoCloseStream(true);
        writerBuilder.head(convertHead(headList)).sheet("sheet1")
                .doWrite(convertData(headList, dataList));
    }


    /**
     * 会先删除excel所有sheet，再写入
     */
    public static <T> void writeSheet(String filePath, String sheetName, Class<T> c, List<T> list) {
        EasyExcel.write(filePath, c).sheet(sheetName).doWrite(list);
    }


    public static <T> List<T> read(String fileName, String sheetName, Class c) {
        List<T> list = new ArrayList();
        EasyExcel.read(fileName, c, new ReadListener<T>() {
            @Override
            public void invoke(T o, AnalysisContext analysisContext) {
                list.add(o);
            }
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
            }
        }).sheet(sheetName).doRead();
        return list;
    }

    public static void main(String[] args) {
        String filePath = "d:\\a.xlsx";

        //表头创建 格式
        List<ExcelHead> headList = new ArrayList();
        headList.add(new ExcelHead<String>("name", "名称"));
        headList.add(new ExcelHead("age", "年龄", -1));
        List<ExcelHead> headList1 = new ArrayList();
        headList1.add(new ExcelHead<String>("name1", "名称1"));
        headList1.add(new ExcelHead("age1", "年龄1", -1));
        headList.add(new ExcelHead("hebing", "合并列", headList1));

        List<ExcelData> list = new ArrayList<>();
        //数据组装格式
        List<Map<String, Object>> dataList = new ArrayList();
        Map<String, Object> one = new HashMap();
        one.put("name", "张三");
        one.put("age", 20);
        dataList.add(one);
        ExcelData e = new ExcelData(dataList);
        list.add(e);

        List<Map<String, Object>> dataList1 = new ArrayList();
        Map<String, Object> two = new HashMap();
        two.put("name", "李四");
        two.put("age", 18);
        dataList1.add(two);
        e = new ExcelData(dataList1);
        list.add(e);

        List<Map<String, Object>> dataList2= new ArrayList();
        Map<String, Object> there = new HashMap();
        there.put("name", "不知道年龄");
        there.put("age", 2);
        dataList2.add(there);
        e = new ExcelData(dataList1);

        Map<String, Object> there1 = new HashMap();
        there1.put("name1", "不知道年龄1");
        there1.put("age1", 2);
        Map<String,Map<String, Object>>maps = new HashMap<>();
        maps.put("hebing",there1);
        e.setSub(maps);
        list.add(e);

        write(filePath, headList, list);
    }

}
