package com.sky.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * ClassName: Project
 * Package: com.sky.entity
 * Description:
 *
 * @Author Tree
 * @Create 2024/4/18 16:42
 * @Version 11
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    //删除标识（0-正常,1-删除）
    private String delFlag;

    //创建时间
    private LocalDateTime createTime;

    private LocalDateTime updateTime;

}
