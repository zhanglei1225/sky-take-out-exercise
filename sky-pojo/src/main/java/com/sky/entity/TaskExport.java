package com.sky.entity;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskExport implements Serializable {

    private static final long serialVersionUID = 1L;

    @ColumnWidth(10)
    @ExcelProperty(index = 0, value = "序号")
    private String no;

    @ColumnWidth(10)
    @ExcelProperty(index = 1, value = "任务名称")
    private String name;

    @ColumnWidth(10)
    @ExcelProperty(index = 2, value = "解决方案")
    private String plan;

    @ColumnWidth(10)
    @ExcelProperty(index = 3, value = "解决时限")
    private String finishTime;

    @ColumnWidth(10)
    @ExcelProperty(index = 4, value = "主办部门/人")
    private String organizer;

    @ColumnWidth(10)
    @ExcelProperty(index = 5, value = "责任领导")
    private String createUsername;

    @ColumnWidth(10)
    @ExcelProperty(index = 6, value = "项目名称")
    private String projectName;

    @ColumnWidth(10)
    @ExcelProperty(index = 7, value = "备注")
    private String remark;
}
