package com.sky.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    // 方案
    private String plan;
    // 任务名称
    private String name;

    // 任务完成时间
    private String finishTime;

    // 未完成原因
    private String unfinishedReason;

    // 预计完成时间
    private String expectTime;

    // 指派人员
    private Integer assignee;

    // 指派人员名称
    private String assigneeName;

    // 状态 1:禁用，0启用
    private Integer delFlag;

    // 主办方
    private String organizer;

    // 等级
    private String level;

    // 进展情况
    private String progress;

    // 批注
    private String comment;

    //任务状态 0:待办  1:已完成 2 未完成
    private Integer status;

    // 项目id
    private Integer project;

    private String projectName;

    //@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    //@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    private Long createUser;

    private String createUsername;

    private Long updateUser;

    private String remark;

}
