package com.sky.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * ClassName: CountTaskDto
 * Package: com.sky.dto
 * Description:
 *
 * @Author Tree
 * @Create 2024/4/18 14:30
 * @Version 11
 */
@Data
public class CountTaskDto implements Serializable {

    private String year;

    private String month;

    private String assignee;
}
