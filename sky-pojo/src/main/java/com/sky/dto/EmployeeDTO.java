package com.sky.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class EmployeeDTO implements Serializable {

    private Long id;

    private String username;

    private String name;

    private String password;

    private String phone;

    private String sex;

    private String idNumber;

    private String role;

    private Integer project;

    // 状态 0:待审核，1:已通过 2:已拒绝
    private Integer processStatus;

}
