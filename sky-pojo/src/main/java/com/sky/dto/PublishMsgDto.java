package com.sky.dto;

import com.sky.entity.Task;
import lombok.Data;

import java.io.Serializable;

@Data
public class PublishMsgDto extends Task {

    private String openId;
    private String accessToken;

}
