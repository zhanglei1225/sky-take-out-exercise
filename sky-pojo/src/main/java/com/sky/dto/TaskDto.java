package com.sky.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskDto  {

    // 任务名称
    private String name;

    // 指派人员名称
    private String assigneeName;

    // 指派人员
    private Integer assignee;

    private Integer current;

    private Integer size;

    private List ids;
}
