package com.sky.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * ClassName: countTaskVo
 * Package: com.sky.vo
 * Description:
 *
 * @Author Tree
 * @Create 2024/4/18 14:36
 * @Version 11
 */
@Data
public class CountTaskVo implements Serializable {
    // 任务总数
    private Integer totalTasks;

    // 已完成
    private Integer completedTasks;

    // 已完成占比
    private String completedTasksRatio;

    // 未完成
    private Integer incompleteTasks;

    // 未完成占比
    private String incompleteTasksRatio;
}
