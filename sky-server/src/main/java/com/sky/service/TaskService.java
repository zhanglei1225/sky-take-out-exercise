package com.sky.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sky.dto.CountTaskDto;
import com.sky.dto.TaskDto;
import com.sky.entity.Task;
import com.sky.vo.CountTaskVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * ClassName: TaskService
 * Package: com.sky.service
 * Description:
 *
 * @Author Tree
 * @Create 2024/4/15 16:58
 * @Version 11
 */
public interface TaskService  extends IService<Task> {
    IPage<Task> getTaskList(Page page,TaskDto task);

    String addTask(Task task);

    CountTaskVo countTask(CountTaskDto countTaskDto);

    void export(TaskDto dto, HttpServletResponse response);

    String importExcel(MultipartFile file);

    String complete(Task task);

    Integer updateTask(Task task);

    Integer deleteTask(Task task);
}
