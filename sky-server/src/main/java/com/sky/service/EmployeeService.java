package com.sky.service;

import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.entity.Role;
import com.sky.result.PageResult;
import com.sky.result.Result;

import java.util.List;
import java.util.Map;

public interface EmployeeService  {

    Employee login(EmployeeLoginDTO employeeLoginDTO);

    PageResult pageQuery(EmployeePageQueryDTO employeePageQueryDTO);

    void startOrStop(Integer status, Long id);

    Result insert(EmployeeDTO employeeDTO);

    Employee getById(Long id);

    void update(EmployeeDTO employeeDTO);

    void processStatus(Integer processStatus, Long id);

    String getOpenId(EmployeeLoginDTO employeeLoginDTO);
}
