package com.sky.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sky.context.BaseContext;
import com.sky.dto.CountTaskDto;
import com.sky.dto.PublishMsgDto;
import com.sky.dto.TaskDto;
import com.sky.entity.Employee;
import com.sky.entity.Project;
import com.sky.entity.Task;
import com.sky.entity.TaskExport;
import com.sky.exception.BaseException;
import com.sky.mapper.EmployeeMapper;
import com.sky.mapper.ProjectMapper;
import com.sky.mapper.TaskMapper;
import com.sky.properties.WeChatProperties;
import com.sky.service.TaskService;
import com.sky.utils.HttpClientUtil;
import com.sky.utils.WeChatPayUtil;
import com.sky.vo.CountTaskVo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * ClassName: TaskServiceImpl
 * Package: com.sky.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2024/4/15 17:00
 * @Version 11
 */
@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements TaskService {
    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private ProjectMapper projectMapper;
    @Autowired
    private WeChatProperties weChatProperties;
    @Autowired
    private WeChatPayUtil weChatPayUtil;
    //获取接口调用凭据地址
    public static final String ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token";

    @Override
    public IPage<Task> getTaskList(Page page, TaskDto task) {
        Long currentId = BaseContext.getCurrentId();
        Employee employee = new Employee();
        if (currentId != null) {
            employee = employeeMapper.getById(currentId);
        }
        String role = employee.getRole();
        LambdaQueryWrapper<Task> wrapper = Wrappers.<Task>lambdaQuery()
//                .like(task.getAssigneeName() != null, Task::getAssigneeName, task.getAssigneeName())
                .eq(task.getAssignee()!= null,Task::getAssignee,task.getAssignee())
                .like(task.getName() != null, Task::getName, task.getName())
                .eq(!"0".equals(role) && employee.getProject()!=null,Task::getProject,employee.getProject())
                .eq(Task::getDelFlag, 0)
                .orderByDesc(Task::getCreateTime);
        return taskMapper.selectPage(page, wrapper);
    }


    @Override
    public String addTask(Task task) {
        task.setCreateTime(LocalDateTime.now());
        task.setUpdateTime(LocalDateTime.now());
        Long currentId = BaseContext.getCurrentId();
        task.setCreateUser(currentId);
        Employee employee = employeeMapper.getById(currentId);
            task.setCreateUsername(employee.getUsername());
        task.setDelFlag(0);
        task.setStatus(0);
        int insert = taskMapper.insert(task);
        String msg =null;
        if (insert>0) {
            // 获取accessToken
            String accessToken = getAccessToken();
            // 推送消息
            PublishMsgDto publishMsgDto = new PublishMsgDto();
            publishMsgDto.setAccessToken(accessToken);
            if (employee.getOpenId()!=null) {
                publishMsgDto.setOpenId(employee.getOpenId());
            }
            BeanUtils.copyProperties(task, publishMsgDto);
            try {
                msg = weChatPayUtil.publishMsg(publishMsgDto);
            } catch (Exception e) {
                throw new BaseException(e.getMessage());
            }
        }
        return msg;
    }

    private String getAccessToken() {
        Map<String, String> map = new HashMap<>();
        map.put("appid", weChatProperties.getAppid());
        map.put("secret", weChatProperties.getSecret());
        map.put("grant_type","client_credential");
        String json = HttpClientUtil.doGet(ACCESS_TOKEN, map); // 4换1，获取json
        //前后端请求响应的都是对象
        JSONObject jsonObject = JSON.parseObject(json); // 字符串转对象
        String accessToken = jsonObject.getString("access_token");// 从对象获取openid属性
        return accessToken;
    }



    @Override
    public Integer updateTask(Task task) {
        task.setUpdateTime(LocalDateTime.now());
        task.setUpdateUser(BaseContext.getCurrentId());
        int i = taskMapper.updateById(task);
        return i;
    }

    @Override
    public Integer deleteTask(Task task) {
        int i = taskMapper.deleteById(task);
        return i;
    }

    @Override
    public String complete(Task task) {
        int i = taskMapper.updateById(task);
        return i+"";
    }

    @Override
    public CountTaskVo countTask(CountTaskDto countTaskDto) {
        CountTaskVo countTaskVo = taskMapper.countTask(countTaskDto);

        return countTaskVo;
// COUNT 函数用于计算指定列中非空值的数量，而在这种情况下，我们需要根据条件进行计数，
// 因此使用 SUM 结合 CASE 表达式更为合适。
    }

    @Override
    public void export(TaskDto dto, HttpServletResponse response) {
        List<TaskExport> taskExports = taskMapper.getExportList(dto);
        for (int i = 0; i < taskExports.size(); i++) {
            taskExports.get(i).setNo(i + 1 + "");
        }

        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("任务清单");

            // 设置第一行样式
            CellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            Font font = workbook.createFont();
            font.setBold(true);
            font.setFontHeightInPoints((short) 16);
            headerStyle.setFont(font);

            // 合并第一行的七列
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));
            Row headerRow = sheet.createRow(0);
            Cell headerCell = headerRow.createCell(0);
            headerCell.setCellValue("工作任务待办清单");
            headerCell.setCellStyle(headerStyle);

            // 创建第二行空白行
            Row blankRow = sheet.createRow(1);
            for (int i = 0; i < 7; i++) {
                blankRow.createCell(i).setCellValue("");
            }

            // 设置标题行样式
            CellStyle titleStyle = workbook.createCellStyle();
            titleStyle.setAlignment(HorizontalAlignment.CENTER);
            titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            titleStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
            titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            Font titleFont = workbook.createFont();
            titleFont.setBold(true);
            titleStyle.setFont(titleFont);

            // 创建标题行
            Row titleRow = sheet.createRow(2);
            String[] titles = {"序号", "任务名称", "解决方案", "解决时限", "主办部门/人", "责任领导", "备注"};
            for (int i = 0; i < titles.length; i++) {
                Cell titleCell = titleRow.createCell(i);
                titleCell.setCellValue(titles[i]);
                titleCell.setCellStyle(titleStyle);
                // 调整列宽以适应内容
//                sheet.autoSizeColumn(i);
            }

            // 写入任务数据
            int rowIndex = 3; // 从第三行开始写入任务数据
            for (TaskExport taskExport : taskExports) {
                Row row = sheet.createRow(rowIndex++);
                row.createCell(0).setCellValue(taskExport.getNo());
                row.createCell(1).setCellValue(taskExport.getName());
                row.createCell(2).setCellValue(taskExport.getPlan());
                row.createCell(3).setCellValue(taskExport.getFinishTime());
                row.createCell(4).setCellValue(taskExport.getOrganizer());
                row.createCell(5).setCellValue(taskExport.getCreateUsername());
                row.createCell(6).setCellValue(taskExport.getRemark());
            }

            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment; filename=\"任务清单.xlsx\"");
            try (OutputStream outputStream = response.getOutputStream()) {
                workbook.write(outputStream);
            }
            System.out.println("Excel 文件导出成功！");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String importExcel(MultipartFile file) {
        List<TaskExport> taskExports = new ArrayList<>();
        try (Workbook workbook = new XSSFWorkbook(file.getInputStream())) {
            Sheet sheet = workbook.getSheetAt(0); // 假设数据在第一个工作表中

            Iterator<Row> rowIterator = sheet.iterator();
            // 跳过前三行，因为第一行是标题，第二行是空白行，第三行是字段名称
            rowIterator.next();
            rowIterator.next();
            rowIterator.next();

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                TaskExport taskExport = new TaskExport();
                /* // 序号不用读取
                // 将数值转化为字符串
                String cellValue = "";
                Cell cell = row.getCell(0);
                if(CellType.NUMERIC.equals(cell.getCellType())) {
                    cellValue = String.valueOf(cell.getNumericCellValue());
                } else if(CellType.STRING.equals(cell.getCellType())) {
                    cellValue = cell.getStringCellValue();
                }
                taskExport.setNo(cellValue);*/
                if (row.getCell(1).getStringCellValue().isEmpty()) {
                    continue;
                }
                taskExport.setName(row.getCell(1).getStringCellValue());
                taskExport.setPlan(row.getCell(2).getStringCellValue());
                String cellValue = "";
                Cell cell = row.getCell(3);
                if (CellType.NUMERIC.equals(cell.getCellTypeEnum())) {
                    if(DateUtil.isCellDateFormatted(cell)) {
                        // 将Excel单元格的日期转换为LocalDateTime
                        Date date = cell.getDateCellValue();
                        /* // 转换Date为LocalDateTime
                        LocalDateTime localDateTime = date.toInstant()
                                //使用系统默认时区
                                .atZone(ZoneId.systemDefault())
                                .toLocalDateTime();
                        cellValue = localDateTime.toString();*/
                        // 格式化日期
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // 创建日期格式化对象
                        cellValue = sdf.format(date); // 格式化日期
                    } else {
                        // 处理为普通数字
                        cellValue = String.valueOf(cell.getNumericCellValue());
                    }
                } else if (CellType.STRING.equals(cell.getCellTypeEnum())) {
                    cellValue = cell.getStringCellValue();
                }
                taskExport.setFinishTime(cellValue);
                taskExport.setOrganizer(row.getCell(4).getStringCellValue());
                taskExport.setCreateUsername(row.getCell(5).getStringCellValue());
                taskExport.setProjectName(row.getCell(6).getStringCellValue());
                taskExport.setRemark(row.getCell(7).getStringCellValue());
                taskExports.add(taskExport);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<Task> tasks = new ArrayList<>();
        for (TaskExport taskExport : taskExports) {
            Task task = new Task();
            BeanUtils.copyProperties(taskExport,task);
            task.setCreateTime(LocalDateTime.now());
            task.setUpdateTime(LocalDateTime.now());
            Long currentId = BaseContext.getCurrentId();
            task.setCreateUser(currentId);
            Employee employee = employeeMapper.getById(currentId);
            if (employee != null) {
                task.setCreateUsername(employee.getUsername());
            }
            task.setDelFlag(0);
            task.setStatus(0);
            tasks.add(task);
            task.setFinishTime(taskExport.getFinishTime());
            Optional.ofNullable(task.getProjectName()).ifPresent(p->
                    {
                        List<Project> projects = projectMapper.selectList(Wrappers.<Project>lambdaQuery().eq(Project::getName, task.getProjectName()));
                        if (projects.size()>1) {
                            throw new BaseException("有重复项目: "+task.getProjectName());
                        }else if (CollectionUtils.isEmpty(projects)){
                            throw new BaseException("项目名称不存在: "+task.getProjectName());
                        }
                        task.setProject(projects.get(0).getId());
                    }
            );
        }
        int rows = taskMapper.insertBatchSelective(tasks);
        if (rows == tasks.size()) {
            // 插入成功
            return "导入成功:"+rows;
        } else {
            // 插入失败或部分成功
            return "导入失败";
        }
    }
}
