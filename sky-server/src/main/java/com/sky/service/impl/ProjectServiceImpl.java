package com.sky.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.sky.entity.Project;

import com.sky.mapper.ProjectMapper;

import com.sky.service.ProjectService;
import com.sky.service.TaskService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * ClassName: TaskServiceImpl
 * Package: com.sky.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2024/4/15 17:00
 * @Version 11
 */
@Service
public class ProjectServiceImpl extends ServiceImpl<ProjectMapper, Project> implements ProjectService {
    @Autowired
    private ProjectMapper projectMapper;

    @Override
    public List<Project> selectList(Project project) {
        return projectMapper.selectList(null);
    }

    @Override
    public void addProject(Project project) {
        project.setCreateTime(LocalDateTime.now());
        project.setUpdateTime(LocalDateTime.now());
        project.setDelFlag("0");
        projectMapper.insert(project);
    }
}
