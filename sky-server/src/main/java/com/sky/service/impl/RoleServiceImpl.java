package com.sky.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sky.entity.Role;
import com.sky.mapper.RoleDao;
import com.sky.service.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Role)表服务实现类
 *
 * @author makejava
 * @since 2024-04-18 17:31:14
 */
@Service("roleService")
public class RoleServiceImpl extends ServiceImpl<RoleDao,Role> implements RoleService {
    @Resource
    private RoleDao roleDao;

    @Override
    public List<Role> getRoleList() {
        return roleDao.getRoleList();
    }
}
