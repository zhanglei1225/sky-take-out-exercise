package com.sky.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sky.dto.CountTaskDto;
import com.sky.entity.Project;
import com.sky.entity.Task;
import com.sky.vo.CountTaskVo;

import java.util.List;

/**
 * ClassName: TaskService
 * Package: com.sky.service
 * Description:
 *
 * @Author Tree
 * @Create 2024/4/15 16:58
 * @Version 11
 */
public interface ProjectService extends IService<Project> {


    List<Project> selectList(Project project);

    void addProject(Project project);
}
