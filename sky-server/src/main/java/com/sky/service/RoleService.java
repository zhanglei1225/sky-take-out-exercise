package com.sky.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.Page;
import com.sky.entity.Role;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * (Role)表服务接口
 *
 * @author makejava
 * @since 2024-04-18 17:31:14
 */
public interface RoleService extends IService<Role> {


    List<Role> getRoleList();

}
