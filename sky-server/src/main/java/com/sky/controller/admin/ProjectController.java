package com.sky.controller.admin;

import com.sky.dto.CountTaskDto;
import com.sky.entity.Project;
import com.sky.entity.Task;
import com.sky.result.Result;
import com.sky.service.ProjectService;
import com.sky.service.TaskService;
import com.sky.vo.CountTaskVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * ClassName: TaskController
 * Package: com.sky.controller.admin
 * Description:
 *
 * @Author Tree
 * @Create 2024/4/15 17:03
 * @Version 11
 */
@RestController
@RequestMapping("/admin/project")
@Slf4j
@Api(tags = "任务相关接口")
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @PostMapping("/list")
    @ApiOperation(value = "项目列表")
    public Result<List<Project>> getList(@RequestBody Project project){
        List<Project> projects = projectService.selectList(project);
        return Result.success(projects);
    }

    @PostMapping("/add")
    @ApiOperation(value = "项目添加")
    public Result<List<Project>> addProject(@RequestBody Project project){
        projectService.addProject(project);
        return Result.success();
    }
}
