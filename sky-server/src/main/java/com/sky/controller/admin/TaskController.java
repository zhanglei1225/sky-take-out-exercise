package com.sky.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sky.dto.CountTaskDto;
import com.sky.dto.TaskDto;
import com.sky.entity.Task;
import com.sky.result.Result;
import com.sky.service.TaskService;
import com.sky.vo.CountTaskVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * ClassName: TaskController
 * Package: com.sky.controller.admin
 * Description:
 *
 * @Author Tree
 * @Create 2024/4/15 17:03
 * @Version 11
 */
@RestController
@RequestMapping("/admin/task")
@Slf4j
@Api(tags = "任务相关接口")
public class TaskController {
    @Autowired
    private TaskService taskService;

    @PostMapping("/list")
    @ApiOperation(value = "待办清单")
    public Result<IPage<Task>> getTaskList(@RequestBody TaskDto task) {
        Page<Task> page = new Page<>(task.getCurrent(),task.getSize());
        IPage<Task> tasks = taskService.getTaskList(page,task);
        return Result.success(tasks);
    }

    @PostMapping("/add")
    @ApiOperation(value = "发布任务")
    public Result addTask(@RequestBody Task task) {
        String flag = taskService.addTask(task);
        return Result.success(flag);
    }

    @PostMapping("/update")
    @ApiOperation(value = "编辑任务")
    public Result updateTask(@RequestBody Task task) {
        Integer flag = taskService.updateTask(task);
        return Result.success(flag);
    }
    @PostMapping("/delete")
    @ApiOperation(value = "删除任务")
    public Result deleteTask(@RequestBody Task task) {
        Integer flag = taskService.deleteTask(task);
        return Result.success(flag);
    }

    @PostMapping("/countTask")
    @ApiOperation(value = "统计任务")
    public Result countTask(@RequestBody CountTaskDto countTaskDto) {
        CountTaskVo countTaskVo = taskService.countTask(countTaskDto);
        return Result.success(countTaskVo);
    }

    @PostMapping("/export")
    @ApiOperation(value = "批量导出", notes = "", httpMethod = "POST")
    public void export(@RequestBody TaskDto dto, HttpServletResponse response) {
        taskService.export(dto, response);
    }

    @ApiOperation(value = "批量导入信息", notes = "", httpMethod = "POST")
    @RequestMapping(value = "/importExcel")
    public Result importExcel(@RequestParam(value = "file", required = true) MultipartFile file) {

        return Result.success(taskService.importExcel(file));
    }

    @PostMapping()
    @ApiOperation(value = "完成/未完成任务")
    public Result complete(@RequestBody Task task) {
        String flag = taskService.complete(task);
        return Result.success(flag);
    }
}
