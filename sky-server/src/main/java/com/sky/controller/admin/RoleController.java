package com.sky.controller.admin;

import com.sky.entity.Role;
import com.sky.result.Result;
import com.sky.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * (Role)表控制层
 *
 * @author makejava
 * @since 2024-04-18 17:31:08
 */
@RestController
@RequestMapping("/admin/role")
@Api(tags = "角色相关接口")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/list")
    @ApiOperation(value = "角色列表")
    public Result<List<Role>> getRoleList(){
        List<Role> roles  = roleService.getRoleList();
        return Result.success(roles);
    }

}

