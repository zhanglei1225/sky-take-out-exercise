package com.sky.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sky.dto.CountTaskDto;
import com.sky.entity.Project;
import com.sky.entity.Task;
import com.sky.vo.CountTaskVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * ClassName: TaskMapper
 * Package: com.sky.mapper
 * Description:
 *
 * @Author Tree
 * @Create 2024/4/15 16:52
 * @Version 11
 */
@Mapper
public interface ProjectMapper extends BaseMapper<Project> {

}
