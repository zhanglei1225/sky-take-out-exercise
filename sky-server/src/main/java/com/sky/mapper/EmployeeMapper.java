package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.annotation.AutoFill;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.entity.User;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface EmployeeMapper  {

    /**
     * 根据用户名查询员工
     * @param username
     * @return
     */
    @Select("select id, name, username, password, role, status, create_user, update_user" +
            " from employee where username = #{username} and status = 1 ")
    Employee getByUsername(String username);


    /**
     * 员工分页查询
     * @param employeePageQueryDTO
     * @return
     */
    Page<Employee> pageQuery(EmployeePageQueryDTO employeePageQueryDTO);

    /**
     * 根据主键动态修改属性
     * @param employee
     */
    @AutoFill(value = OperationType.UPDATE)
    void update(Employee employee);

    @Insert("insert into employee(name, username, password, role,project, create_time," +
            " update_time, create_user, update_user,status,process_status) VALUES (#{name},#{username},#{password}," +
            "#{role},#{project},#{createTime},#{updateTime},#{createUser},#{updateUser},#{status},#{processStatus})")
    @AutoFill(value = OperationType.INSERT)
    void insert(Employee employee);

    @AutoFill(value = OperationType.INSERT)
    void insertEmployee(Employee employee);

    @Select("select * from employee where id =#{id}")
    Employee getById(Long id);


    @Select("select * from employee where openid =#{openid}")
    Employee getByOpenid(String openid);

}
