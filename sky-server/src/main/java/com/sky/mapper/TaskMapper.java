package com.sky.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sky.dto.CountTaskDto;
import com.sky.dto.TaskDto;
import com.sky.entity.Task;
import com.sky.entity.TaskExport;
import com.sky.vo.CountTaskVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * ClassName: TaskMapper
 * Package: com.sky.mapper
 * Description:
 *
 * @Author Tree
 * @Create 2024/4/15 16:52
 * @Version 11
 */
@Mapper
public interface TaskMapper extends BaseMapper<Task> {
    CountTaskVo countTask(CountTaskDto countTaskDto);

    List<TaskExport> getExportList(TaskDto dto);

    Integer insertBatchSelective(ArrayList<Task> tasks);
}
