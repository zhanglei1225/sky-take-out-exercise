package com.sky.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sky.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * (Role)表数据库访问层
 *
 * @author makejava
 * @since 2024-04-18 17:31:12
 */
@Mapper
public interface RoleDao extends BaseMapper<Role> {

    @Select("select id,name from role where del_flag = '0'")
    List<Role> getRoleList();

}

